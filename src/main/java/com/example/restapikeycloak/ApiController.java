package com.example.restapikeycloak;

import org.apache.http.entity.ContentType;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.token.TokenManager;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.account.UserRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
public class ApiController {
    
    private Keycloak keycloak_1;
    private Keycloak keycloak_2;
    private String clientSecret_1;
    private String clientSecret_2;
    private TokenManager tokenManager_1;
    private TokenManager tokenManager_2;
    private String accessToken_1;
    private String accessToken_2;

    private String tokenInClass;

    public ApiController() {
        KeycloakConfig keycloakConfig = new KeycloakConfig();

        keycloak_1 = keycloakConfig.keycloak_1();
        tokenManager_1 = keycloak_1.tokenManager();
        accessToken_1 = tokenManager_1.getAccessTokenString();
        System.out.println(accessToken_1);

        keycloak_2 = keycloakConfig.keycloak_2();
        tokenManager_2 = keycloak_2.tokenManager();
        accessToken_2 = tokenManager_2.getAccessTokenString();
        System.out.println(accessToken_2);

        clientSecret_1 = keycloakConfig.getClientSecret();
        clientSecret_2 = keycloakConfig.getClientSecret2();
    }

    public String getAccessToken_1() {
        return tokenManager_1.getAccessTokenString();
    }

    public String getAccessToken_2() {
        return tokenManager_2.getAccessTokenString();
    }

    public HashMap<String, Object> stringToJson(String jsonString) throws IOException {
		//String jsonString = "{\"exp\":1636890985,\"iat\":1636890685}";
		ObjectMapper mapper = new ObjectMapper();
		
		return mapper.readValue(jsonString, new TypeReference<HashMap<String,Object>>(){});
	}

    //@ResponseBody
    @GetMapping("/hello")
    public String test_0()
    {
        accessToken_1 = getAccessToken_1();
        accessToken_2 = getAccessToken_2();

        return "Hello!\n" 
            + "stoh-service : " + "\n"+accessToken_1 
            + "\ntest-realm : " + "\n"+accessToken_2
            + "\n"+keycloak_1.realms().toString();
    }

    /*@PostMapping("/token")
    public String token_1(String userName, String password) {
        String token = "";
        keycloak_1.realms().toString()
        return token;
    }//*/

    @GetMapping("/validate")
    public String test_1(@RequestParam(value="token", defaultValue="") String token) {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/x-www-form-urlencoded");

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("client_id", "admin-spring-boot");
        params.add("client_secret", this.clientSecret_1);//"80af9f52-a927-4f06-bc42-03ab224d067b");
        params.add("token", token);

        System.out.println("token="+token);

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);

        RestTemplate rt = new RestTemplate();
        ResponseEntity<String> response = rt.exchange("http://localhost:8080/auth/realms/stoh-service/protocol/openid-connect/token/introspect",
                    HttpMethod.POST,
                    entity,
                    String.class);

        String body = response.getBody();

        // TODO : JSON parsing required to find the value of "active"
        HashMap<String,Object> parsed = new HashMap<String, Object>();
        try {
			parsed = stringToJson(body);
		} catch(JsonParseException e) {	
		} catch(IOException e) {}

        System.out.println("active="+parsed.get("active"));

        return body;
    }

    @PostMapping("/validateToken")
    //public String test_2() {
    public String test_2(@RequestParam(value="token", defaultValue="") String token) {
        
        //String token="eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ4eDE0eWM1R3V2Q0RPbTlTUlNzM3NKQ1RNbGFqWHF5d3dTaHcwU1VjbURZIn0.eyJleHAiOjE2MzkzMDQ5NTAsImlhdCI6MTYzOTMwNDY1MCwianRpIjoiMzk1NmQ2OWMtMTdjZC00Nzk0LTg1NTAtZGY3ODI1NjdmZDQ3IiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwL2F1dGgvcmVhbG1zL3N0b2gtc2VydmljZSIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiJjYTk5Mjk5YS0xMTc3LTQxMzQtODI1NC01MjRhNTlmNjljMDUiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJhZG1pbi1zcHJpbmctYm9vdCIsInNlc3Npb25fc3RhdGUiOiIwM2VmNjcyZC03ODI3LTQ2ODAtODdhOC1mNzJlOTQ1ODY1NzUiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiZGVmYXVsdC1yb2xlcy1zdG9oLXNlcnZpY2UiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoiZW1haWwgcHJvZmlsZSIsInNpZCI6IjAzZWY2NzJkLTc4MjctNDY4MC04N2E4LWY3MmU5NDU4NjU3NSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwicHJlZmVycmVkX3VzZXJuYW1lIjoidXNlcjAxIn0.vwpWCxud3O5FZGR5bjpNoBeR4bLPGutssXPVmguLA6LmakMiGkNIxkiQ2dawMDBCKQY3IrDYRVikWD4005FbJpze8vNl0C5GtLsU1kkufdagGxjIKxqmE0FDXBdh6B2XIhb1M0j8YTxrrxZ0lfTDhV3yCFQe8rPh4qczOoai7r7BslZBeiwpFr9xe27q1OaD0IZLFBRtl3-olOfh685ymhIzH5hLi6tCoHX59wJQwdEIGWOz1SbCWWuvUPayUDlgQ_oFrfaVr2BoIPz1VRvBIpUP-7mOulPSWt2Fes7NZiE-o1nSZfXF2eBzp_1wCA9O3xJHGAnYuMhdWnfKCDHnKw";
        System.out.println("validate2--> test2 function");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/x-www-form-urlencoded");

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("client_id", "admin-spring-boot");
        params.add("client_secret", this.clientSecret_1);//"80af9f52-a927-4f06-bc42-03ab224d067b");
        params.add("token", token);

        System.out.println("token="+token);

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);

        RestTemplate rt = new RestTemplate();
        ResponseEntity<String> response = rt.exchange("http://localhost:8080/auth/realms/stoh-service/protocol/openid-connect/token/introspect",
                    HttpMethod.POST,
                    entity,
                    String.class);

        String body = response.getBody();

        // TODO : JSON parsing required to find the value of "active"
        HashMap<String,Object> parsed = new HashMap<String, Object>();
        try {
			parsed = stringToJson(body);
		} catch(JsonParseException e) {	
		} catch(IOException e) {}

        System.out.println("active="+parsed.get("active"));

        return body;
    }

    @PostMapping("/getToken")
    public ResponseEntity<String> test_3(@RequestParam(value="username", defaultValue="") String username,
                         @RequestParam(value="password", defaultValue="") String password) {
        
        System.out.println("getToken");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/x-www-form-urlencoded");

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("client_id", "admin-spring-boot");
        params.add("client_secret", this.clientSecret_1);//"80af9f52-a927-4f06-bc42-03ab224d067b");
        params.add("username", username);
        params.add("password", password);
        params.add("grant_type", "password");

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);

        RestTemplate rt = new RestTemplate();
        ResponseEntity<String> response;
        try { 
                response = rt.exchange("http://localhost:8080/auth/realms/stoh-service/protocol/openid-connect/token",
                                       HttpMethod.POST,
                                       entity,
                                       String.class);
        } catch(Exception e) {
            return new ResponseEntity<String>("{\"error\": \"Authentication failed\"}", HttpStatus.UNAUTHORIZED);
        }

        String body = response.getBody();
        System.out.println(response.getStatusCode()+" "+response.getStatusCodeValue());
        //if( response.getStatusCodeValue() >= 300 )
        //{
        //    return new ResponseEntity<String>(HttpStatus.FORBIDDEN);
        //}
        
        // TODO : JSON parsing required to find the value of "active"
        HashMap<String,Object> parsed = new HashMap<String, Object>();
        try {
			parsed = stringToJson(body);
		} catch(JsonParseException e) {	
		} catch(IOException e) {}

        System.out.println("expires_in="+parsed.get("expires_in"));

        return response;
    }
}
