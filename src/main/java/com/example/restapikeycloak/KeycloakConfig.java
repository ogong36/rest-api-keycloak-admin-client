package com.example.restapikeycloak;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;//*/

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Configuration
public class KeycloakConfig {

    private String serverUrl = "http://localhost:8080/auth";
    private String realm = "stoh-service";
    private String clientId = "admin-spring-boot";
    private String clientSecret = "c28bf86a-9adc-4da2-abe3-9c8ccc6c9499";

    private String serverUrl2 = "http://localhost:8080/auth";
    private String realm2 = "test-realm";
    private String clientId2 = "admin-spring-boot";
    private String clientSecret2 = "80af9f52-a927-4f06-bc42-03ab224d067b";

    @Bean
    public Keycloak keycloak_1() {
        System.out.println(serverUrl+"\n"+realm+"\n"+clientId+"\n"+clientSecret);

        return KeycloakBuilder.builder()
                .serverUrl(serverUrl)
                .realm(realm)
                .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                .clientId(clientId)
                .clientSecret(clientSecret)
                //.resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
                .build();
    }

    @Bean
    public Keycloak keycloak_2() {
        System.out.println(serverUrl2+"\n"+realm2+"\n"+clientId2+"\n"+clientSecret2);

        return KeycloakBuilder.builder()
                .serverUrl(serverUrl2)
                .realm(realm2)
                .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                .clientId(clientId2)
                .clientSecret(clientSecret2)
                //.resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
                .build();
    }
}
