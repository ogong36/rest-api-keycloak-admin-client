package com.example.restapikeycloak;

import java.util.Arrays;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.token.TokenManager;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class RestapikeycloakApplication {

	KeycloakConfig keycloakConfig = new KeycloakConfig();

	public static void main(String[] args) {
		SpringApplication.run(RestapikeycloakApplication.class, args);
	}

	/*
	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			
			Keycloak keycloak_1 = keycloakConfig.keycloak_1();
			TokenManager tokenManager_1 = keycloak_1.tokenManager();
			String accessToken_1 = tokenManager_1.getAccessTokenString();
			System.out.println(accessToken_1);

			Keycloak keycloak_2 = keycloakConfig.keycloak_2();
			TokenManager tokenManager_2 = keycloak_2.tokenManager();
			String accessToken_2 = tokenManager_2.getAccessTokenString();
			System.out.println(accessToken_2);
		};
	}//*/

}
